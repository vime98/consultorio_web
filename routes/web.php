<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/pacientes')->name('paciente.')->namespace('paciente')->group(function(){
    //All the admin routes will be defined here...
    Route::get('/paciente/dashboard','paciente\HomeController@index')->name('paciente.home');
    Route::get('/dashboard','HomeController@index')->name('home');
    Route::namespace('Auth')->group(function(){

        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('logout');

        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');
    });
  });
  Route::name('VerMiExpediente')->get('verExpe/{id}', 'PacientesController@verExpe');
  Route::name('showMiExpediente')->get('showExpe/{id}', 'PacientesController@showExpe');


Route::resource('usuarios', 'UsuariosController');
Route::get('paciente/ayuda/{id}', 'PacientesController@ayuda');
Route::get('paciente/paccomp', 'PacientesController@paccomp');
Route::resource('paciente', 'PacientesController');
Route::resource('notas', 'NotasController');


Route::name('CrearExpe')->get('historia_clinica/CrearExpe/{id}','HistoriaClinicaController@CrearExpe');
Route::name('VerExpe')->get('historia_clinica/VerExpe/{id}','HistoriaClinicaController@VerExpe');
Route::name('imprimir')->get('/imprimir/{id}', 'HistoriaClinicaController@imprimir');
Route::resource('historia_clinica', 'HistoriaClinicaController');


Route::name('CrearNE')->get('NoEvu/CrearNE/{id}','Nota_EvolucionController@Crear_NE');
Route::name('print')->get('print/NE/{id}','Nota_EvolucionController@imprimir');
Route::resource('NoEvu', 'Nota_EvolucionController');
