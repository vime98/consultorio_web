<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pacientes()
    {
        return $this->hasMany('App\Pacientes');
    }

    public function histocli()
    {
        return $this->hasOne('App\Historia_Clinica');
    }
    public function NoEv()
    {
        return $this->hasOne('App\Nota_Evolucion');
    }

    public function P1()
    {
        return $this->belongsToMany('App\Pacientes','historia_clinica','user_id','pacientes_id')
        ->withPivot('id','Nombre','Edad','Direccion','Ante_AbuelosP',
        "Ante_AbuelosP" ,
        "Ante_AbuelosM" ,
        "Ante_Tios" ,
        "Ante_Papa" ,
        "Ante_Mama" ,
        "Ante_Hermanos" ,
        "Fecha_Nacimiento" ,
        "Lugar_Nacimiento" ,
        "Residencia_actual" ,
        "Escolaridad" ,
        "Estado_Civil" ,
        "Habitos_Higiene" ,
        "Habitos_Dieta" ,
        "Toxicomanias" ,
        "Enferme_infancia" ,
        "Cirugias" ,
        "Alergias" ,
        "Medica_Actual" ,
        "Menarca" ,
        "Ritmo_Menstrual" ,
        "FUM" ,
        "Gestas" ,
        "Paras" ,
        "Abortos" ,
        "Cesareas" ,
        "FUP" ,
        "USA" ,
        "Anticonseptivos" ,
        "Peso" ,
        "Talla" ,
        "Cabeza" ,
        "Ojos" ,
        "Oidos" ,
        "Nariz" ,
        "Boca" ,
        "Cuello" ,
        "Torax" ,
        "Forma" ,
        "Mov_Respira" ,
        "FR" ,
        "Ruido_Anormal" ,
        "FC" ,
        "Abdomen" ,
        "Extre_superior" ,
        "Extre_inferior" ,
        "created_at" ,
        "updated_at" );
    }

    public function NE1()
    {
        return $this->belongsToMany('App\Pacientes', 'nota_evolucion','doctor_id','paciente_id' )
        ->withPivot('id','paciente_id','doctor_id','Sintomas','Diagnostico','Tratamiento','Signos',"created_at" ,"updated_at" )
        ;
    }
}
