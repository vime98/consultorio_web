<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pacientes extends Authenticatable
{
    //
    protected $table = "pacientes";
    protected $primaryKey = 'id';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'Nombre', 'Apellido_Paterno', 'Apellido_Materno','email', 'password', 'compartido','user_id',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function historiaclinica()
    {
        return $this->hasMany('App\Historia_Clinica');
    }

    public function NoEv()
    {
        return $this->hasMany('App\Nota_Evolucion');
    }
    //Lit solo uso estos xd
    public function D1()
    {
        return $this->belongsToMany('App\User', 'historia_clinica','pacientes_id','user_id' )
        ->withPivot('id','Nombre','Edad','Direccion','Ante_AbuelosP',
        "Ante_AbuelosP" ,
        "Ante_AbuelosM" ,
        "Ante_Tios" ,
        "Ante_Papa" ,
        "Ante_Mama" ,
        "Ante_Hermanos" ,
        "Fecha_Nacimiento" ,
        "Lugar_Nacimiento" ,
        "Residencia_actual" ,
        "Escolaridad" ,
        "Estado_Civil" ,
        "Habitos_Higiene" ,
        "Habitos_Dieta" ,
        "Toxicomanias" ,
        "Enferme_infancia" ,
        "Cirugias" ,
        "Alergias" ,
        "Medica_Actual" ,
        "Menarca" ,
        "Ritmo_Menstrual" ,
        "FUM" ,
        "Gestas" ,
        "Paras" ,
        "Abortos" ,
        "Cesareas" ,
        "FUP" ,
        "USA" ,
        "Anticonseptivos" ,
        "Peso" ,
        "Talla" ,
        "Cabeza" ,
        "Ojos" ,
        "Oidos" ,
        "Nariz" ,
        "Boca" ,
        "Cuello" ,
        "Torax" ,
        "Forma" ,
        "Mov_Respira" ,
        "FR" ,
        "Ruido_Anormal" ,
        "FC" ,
        "Abdomen" ,
        "Extre_superior" ,
        "Extre_inferior" ,
        "created_at" ,
        "updated_at" );
    }

    public function NE1()
    {
        return $this->belongsToMany('App\User', 'nota_evolucion','paciente_id','doctor_id' )
        ->withPivot('id','paciente_id','doctor_id','Sintomas','Diagnostico','Tratamiento','Signos',"created_at" ,"updated_at" )
        ->orderBy('nota_evolucion.created_at', 'desc');
    }


}
