<?php

namespace App\Http\Controllers;

use App\Historia_Clinica;
use App\Http\Controllers\Controller;
use App\Nota_Evolucion;
use App\Pacientes;
use Barryvdh\DomPDF\Facade as PDF;
use App\User;
use Illuminate\Http\Request;

class HistoriaClinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    public function VerExpe($id)
    {
        //
        $paciente=Pacientes::find($id);
        $paciente->D1;

        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        /*foreach ($pac_NE->NE1 as $key => $value) {
            echo "<p>".$value->pivot->created_at."</p>";
        }
        dd($pac_NE);*/

        /*$paciente=Pacientes::find($id);
        $paciente->historiaclinica;

        $histo=Historia_Clinica::find(3);
        $histo->Doctor;
        dd($paciente);

        $doctores= User::where('rol_id','3')->get();*/

        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CrearExpe($id)
    {
        //
        $paciente=Pacientes::find($id);
        return view('HistoriaClinica.create',compact('paciente'));
    }


    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos=request()->except('_token');
        Historia_Clinica::create($datos);
        $id = $request['pacientes_id'];
        $paciente=Pacientes::find($id);
        $paciente->historiaclinica;
        $paciente->D1;

        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Historia_Clinica  $historia_Clinica
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $paciente=Historia_Clinica::find($id);
        return view('HistoriaClinica.show',compact('paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Historia_Clinica  $historia_Clinica
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $paciente=Historia_Clinica::find($id);
        return view('HistoriaClinica.edit',compact('paciente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Historia_Clinica  $historia_Clinica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historia_Clinica $historia_Clinica)
    {
        //
        $datos=$request->except(['_token', '_method']);
        $users = Historia_Clinica::find($datos['id']);

        $users->Nombre = $request->Nombre;
        $users->Edad = $request->Edad;
        $users->Direccion = $request->Direccion;
        $users->Ante_AbuelosP = $request->Ante_AbuelosP;
        $users->Ante_AbuelosM = $request->Ante_AbuelosM;
        $users->Ante_Tios = $request->Ante_Tios;
        $users->Ante_Papa = $request->Ante_Papa;
        $users->Ante_Mama = $request->Ante_Mama;
        $users->Ante_Hermanos = $request->Ante_Hermanos;

        $users->Fecha_Nacimiento = $request->Fecha_Nacimiento;
        $users->Lugar_Nacimiento = $request->Lugar_Nacimiento;
        $users->Residencia_actual = $request->Residencia_actual;
        $users->Escolaridad = $request->Escolaridad;
        $users->Estado_Civil = $request->Estado_Civil;
        $users->Habitos_Higiene = $request->Habitos_Higiene;
        $users->Habitos_Dieta = $request->Habitos_Dieta;
        $users->Toxicomanias = $request->Toxicomanias;
        $users->Enferme_infancia = $request->Enferme_infancia;

        $users->Cirugias = $request->Cirugias;
        $users->Alergias = $request->Alergias;
        $users->Medica_Actual = $request->Medica_Actual;
        $users->Menarca = $request->Menarca;
        $users->Ritmo_Menstrual = $request->Ritmo_Menstrual;
        $users->FUM = $request->FUM;
        $users->Gestas = $request->Gestas;
        $users->Paras = $request->Paras;
        $users->Abortos = $request->Abortos;
        $users->Cesareas = $request->Cesareas;

        $users->FUP = $request->FUP;
        $users->USA = $request->USA;
        $users->Anticonseptivos = $request->Anticonseptivos;
        $users->Peso = $request->Peso;
        $users->Talla = $request->Talla;
        $users->Cabeza = $request->Cabeza;
        $users->Ojos = $request->Ojos;
        $users->Oidos = $request->Oidos;
        $users->Nariz = $request->Nariz;
        $users->Boca = $request->Boca;

        $users->Cuello = $request->Cuello;
        $users->Torax = $request->Torax;
        $users->Forma = $request->Forma;
        $users->Mov_Respira = $request->Mov_Respira;
        $users->FR = $request->FR;
        $users->Ruido_Anormal = $request->Ruido_Anormal;
        $users->FC = $request->FC;
        $users->Abdomen = $request->Abdomen;
        $users->Extre_superior = $request->Extre_superior;
        $users->Extre_inferior = $request->Extre_inferior;

        $users->save();

        $id = $datos['pacientes_id'];
        $paciente=Pacientes::find($id);
        $paciente->D1;
        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'))
                        ->with('success','Usuario actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Historia_Clinica  $historia_Clinica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Historia_Clinica::find($id)->delete();
        $paciente=Pacientes::find($request['id_pacc']);
        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'))
                        ->with('success','Product deleted successfully');
        //
    }
    public function imprimir($id)
    {
        $paciente=Historia_Clinica::find($id);
        $pdf = PDF::loadView('PDFs.expediente',compact('paciente'));
        return $pdf->download('Expediente_Medico_DE:'.$paciente->Nombre.'_'.$paciente->ApellidoPaterno.'_'.$paciente->ApellidoMaterno.'.pdf');
        //return $pdf->stream();
        //
    }
}
