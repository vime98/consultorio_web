<?php

namespace App\Http\Controllers;

use App\Pacientes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users=User::find(Auth::id());
        $users->pacientes;
        return view('pacientes.index',compact('users'));
    }

    public function verExpe($id)
    {
        //
        $paciente=Pacientes::find($id);
        $paciente->D1;

        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        return view('pacientes.expediente',compact('paciente'),compact('pac_NE'));
    }



    public function paccomp(Request $request)
    {
        //
        $pacientes = Pacientes::all();
        return view('pacientes.show_other_pac',compact('pacientes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Conocer la confirmacino
        if ($request['compartido'] == "on") {
           $compartido = 1;
        }
        else{
            $compartido = 0;
        }

        $users = Pacientes::create([
            'Nombre' => $request['Nombre'],
            'Apellido_Paterno' => $request['Apellido_Paterno'],
            'Apellido_Materno' => $request['Apellido_Materno'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'compartido' => $compartido,
            'user_id' => $request['user_id'],
        ]);

        return redirect()->route('paciente.index')
                        ->with('success','Usuario creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $paciente = Pacientes::find($id);
        return view('pacientes.view',compact('paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //http://localhost/consultorio/public/paciente/897/edit
        $users = Pacientes::findOrfail($id);
        return view('pacientes.edit',compact('users'));
    }

    public function ayuda($id)
    {
        $paciente=Pacientes::find($id);
        return view('pacientes.confirmacion',compact('paciente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $compartido = $request['compartido'];
        if ($compartido == "on") {
            $compartido = 1;
         }
         else{
             $compartido = 0;
         }

        $users = Pacientes::findorfail($id);
        $users->Nombre =  $request->Nombre;
        $users->email = $request->email;
        $users->compartido = $compartido;
        $users->save();

        return redirect()->route('paciente.index')
                        ->with('success','Usuario actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Pacientes::find($id)->delete();
        return redirect()->route('paciente.index')
                        ->with('success','Product deleted successfully');
    }
}
