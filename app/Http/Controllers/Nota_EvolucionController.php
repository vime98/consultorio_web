<?php

namespace App\Http\Controllers;

use App\Nota_Evolucion;
use App\Pacientes;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class Nota_EvolucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Crear_NE($id)
    {
        //
        $paciente=Pacientes::find($id);
        return view('Notas_evolucion.create',compact('paciente'));
    }

    public function create()
    {
        //
        return view('Notas_evolucion.create',compact('paciente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos=request()->except('_token');
        Nota_Evolucion::create($datos);
        $id = $request['paciente_id'];
        $paciente=Pacientes::find($id);
        $paciente->historiaclinica;

        $pac_NE=Pacientes::find($id);
        $pac_NE->NE1;

        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nota_Evolucion  $nota_Evolucion
     * @return \Illuminate\Http\Response
     */
    public function show(Nota_Evolucion $nota_Evolucion)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nota_Evolucion  $nota_Evolucion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $nota=Nota_Evolucion::find($id);
        return view('Notas_Evolucion.edit',compact('nota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nota_Evolucion  $nota_Evolucion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nota_Evolucion $nota_Evolucion)
    {
        //
        $datos=$request->except(['_token', '_method']);
        $Nota = Nota_Evolucion::find($datos['id']);

        $Nota->Sintomas = $request->Sintomas;
        $Nota->Diagnostico = $request->Diagnostico;
        $Nota->Tratamiento = $request->Tratamiento;
        $Nota->Signos = $request->Signos;

        $Nota->save();
        $paciente=Pacientes::find($request['id_pacc']);
        $paciente->historiaclinica;
        $paciente->D1;
        $pac_NE=Pacientes::find($request['id_pacc']);
        $pac_NE->NE1;
        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'))
                        ->with('success','Usuario actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nota_Evolucion  $nota_Evolucion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        Nota_Evolucion::find($id)->delete();
        $paciente=Pacientes::find($request['id_pacc']);
        $paciente->historiaclinica;
        $paciente->D1;
        $pac_NE=Pacientes::find($request['id_pacc']);
        $pac_NE->NE1;
        return view('Notas_evolucion.index',compact('paciente'),compact('pac_NE'))
        ->with('success','Product deleted successfully');;
    }

    public function imprimir($id)
    {
        $nota=Nota_Evolucion::find($id);
        $paciente = Pacientes::find($nota->paciente_id);
        $Doc_di = User::find($nota->doctor_id);
        $pdf = PDF::loadView('PDFs.NotaEv',compact('nota','Doc_di','paciente'));
        return $pdf->download('Nota de Evolucion de:'.$paciente->Nombre.'_'.$paciente->ApellidoPaterno.'_'.$paciente->ApellidoMaterno.'.pdf');
        //return $pdf->stream();
        //
    }
}
