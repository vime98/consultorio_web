<?php

namespace App\Http\Controllers;

use App\Usuarios;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::join('rol','users.rol_id','rol.id')
        ->select('users.*','Nombre_rol')->get();

        return view('auth.consultar',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::insert([
            'Nombre' => $request['Nombre'],
            'Apellido_Paterno' => $request['Apellido_Paterno'],
            'Apellido_Materno' => $request['Apellido_Materno'],
            'Edad' => $request['Edad'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'rol_id' => $request['rol_id'],
            'created_at' => today(),
        ]);
        return redirect()->route('usuarios.index')
        ->with('success','Usuario creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {               
        $users = User::findOrfail($id);
        $roles = DB::table('rol')->select('id','Nombre_rol')->get();

        return view('auth.edit',compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::findorfail($id);
        $users->Nombre =  $request->Nombre;
        $users->Apellido_Paterno =  $request->Apellido_Paterno;
        $users->Apellido_Materno = $request->Apellido_Materno;
        $users->Edad =  $request->Edad;
        $users->email =  $request->email;
        $users->password =  $request->password;
        $users->rol_id =  $request->rol_id;
        $users->save();

        return redirect()->route('usuarios.index')
                        ->with('success','Usuario actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('usuarios.index')
                        ->with('success','Product deleted successfully');
    }
}
