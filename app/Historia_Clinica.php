<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historia_Clinica extends Model
{
    //
    protected $table = 'historia_clinica';
    protected $primaryKey = 'id';

    public function paciente()
    {
        return $this->belongsTo('App\Pacientes');
    }

    public function doctor()
    {
        return $this->belongsTo('App\User' ,'user_id' );
    }

    protected $fillable = [
        'Nombre','pacientes_id','user_id','Edad','Direccion',
    ];
}
