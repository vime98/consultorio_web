@extends('layouts.app')
@section('content')
<header class="page-header">
    <br>
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Historia Clinica</h2>
        </div>
        <div class="pull-right">
            <a href="{{ url('paciente')}}" class="btn btn-primary">Regresar a Mis pacientes</a>
        </div>
        <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ url('paciente/paccomp') }}">Regresar a todos los Pacientes </a>
        </div>
    </div>
  </header>
  <br>
  <div class="container">
    <div class="form-group">
        <div class="justify-content-between align-items-center row">
            <div class="col-md-4 mx-auto">
               <center><div class="card" style="max-width: 300px;">
                    @if ($paciente->Sexo == "M" )
                      <img src="{{ asset('images/M1.jpg') }}" class="card-img-top" alt="Card image cap">
                      @else
                      <img src="{{ asset('images/F1.png') }}" class="card-img-top" alt="Card image cap">
                      @endif
                    <div class="card-body">
                      <h5 class="card-title">Paciente: &nbsp;{{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </h5>
                      <p class="card-text">Correo: {{$paciente->email}}</p>
                      @if ($paciente->Sexo == "F" )
                      <p class="card-text">Sexo: Femenino</p>
                      @else
                      <p class="card-text">Sexo: Masculino</p>
                      @endif
                      <p class="card-text"><small class="text-muted">Fecha de alta: {{ $paciente->created_at->toFormattedDateString() }}</small></p>
                    </div>
                  </div>
                  <br>
            </div>
                @forelse ($paciente->D1 as $Doctor)
                <div class="col-md-8 mx-auto">
                <center>
                    <!-- here esta la card-->
                    <div class="card mb-3"  style="max-width: 540px; background-color:currentColor">
                        <!-- Card content -->
                        <div class="card-body elegant-color white-text rounded-bottom">
                        <!-- Social shares button -->
                        <a class="activator waves-effect mr-4" style="color:floralwhite"><i class="far fa-user"></i></a>
                        <!-- Title -->
                        <h4 class="card-title" style="color:floralwhite ; font-family: Courier New" >Doctor que registro el Expediente: &nbsp;{{ $Doctor->Nombre }} {{ $Doctor->Apellido_Paterno }} {{ $Doctor->Apellido_Materno }}</h4>
                        <hr class="hr-light" style="background-color: floralwhite">
                        <!-- Text -->
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Datos Expediente Paciente:</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Edad: {{$Doctor->pivot->Edad}} </p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Direccion: {{$Doctor->pivot->Direccion}} </p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Doctor Principal : {{ $paciente->user->Nombre }} {{ $paciente->user->Apellido_Paterno }} {{ $paciente->user->Apellido_Materno }}</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Fecha de registro del Paciente : {{ $paciente->created_at->toFormattedDateString() }}</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Tiempo que tiene el expediente : {{ $Doctor->pivot->created_at->diffForHumans() }}</p>
                            <!-- <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Hora de registro : {{ $paciente->created_at->diffForHumans() }}</p> -->
                        <!-- Link  href="{{url ('entrenadores/ayuda2',$paciente->id)}}"-->
                        <hr class="hr-light" style="background-color: floralwhite">
                        <div class="justify-content-between align-items-center row">
                            <div class="col-md-4 mx-auto">
                                <a href="{{ route('historia_clinica.show',$Doctor->pivot->id) }}" class="white-text d-flex justify-content-end" style="color: floralwhite ; font-family: Courier New">
                                    <h5>Ver Expediente <i class="far fa-eye"></i></h5>
                                </a>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <a href="{{ route('historia_clinica.edit',$Doctor->pivot->id) }}" class="white-text d-flex justify-content-end" style="color: floralwhite ; font-family: Courier New">
                                    <h5>Editar Expediente <i class="far fa-edit"></i></h5>
                                </a>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <a href="#" class="white-text d-flex justify-content-end" style="color: floralwhite ; font-family: Courier New" data-toggle="modal" data-target="#staticBackdrop{{$Doctor->id}}" >
                                    <h5>Borrar Expediente <i class="fas fa-trash-alt"></i></h5>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                @empty
                <br>
                <center><div class="col-md-8 mx-auto">
                    <p>No hay Expedientes</p>
                    <a class="btn btn-primary" href="{{ route('CrearExpe',$paciente->id) }}">Agregar Expediente</a>
                </div>
                @endforelse
        </div>
        <br>
    </div>
  </div>
  <div clas="container">
    <div class=" row row-cols-1 row-cols-md-3 row-cols-lg-4" >
      @forelse ($pac_NE->NE1 as $Nose)
      <div class="col mb-4" style="max-width: 100rem;" id="myDIV">
        <div class="card border-warning">
            <div class="card-header">Nota Evolutiva</div>
        <div class="card-body">
            <h5 class="card-title">Diagnosticado por: <strong>{{$Nose->Nombre}} {{$Nose->Apellido_Paterno}} {{$Nose->Apellido_Materno}}</strong></h5>
            <p class="card-text">Informacion:</p>
            <p class="card_text">Doctor Principal : {{ $paciente->user->Nombre }} {{ $paciente->user->Apellido_Paterno }} {{ $paciente->user->Apellido_Materno }}</p>
            <p class="card-text">Fecha de Ultima Consulta : {{ $Nose->pivot->created_at->toFormattedDateString() }}</p>
            <p class="card-text">Ultima Consulta : {{ $Nose->pivot->created_at->diffForHumans() }}</p>
            <div class="d-inline-flex">
                <a class="btn btn-light" data-toggle="modal" data-target="#viewData{{$Nose->pivot->id}}">
                    <i class="fa fa-eye"></i></a>
                &nbsp;
                <a class="btn btn-warning" href="{{ route('NoEvu.edit',$Nose->pivot->id) }}">Editar</a>
                &nbsp;
                @if(auth()->user()->rol_id=='2')
                <button class="btn btn-danger" data-toggle="modal" data-target="#nota{{$Nose->pivot->id}}" >Borrar</button>
                @endif
              </div>
        </div>
        </div>
    </div>
      @empty
      <center><div class="col mb-4">
        <p>No hay Consultas</p>
        <a class="btn btn-primary" href="{{ route('CrearNE',$paciente->id) }}">Agregar Notas de Evolucion</a>
    </div>
      @endforelse
    </div>
    </div>
<center><div class="col mb-4">
    <a class="btn btn-primary" href="{{ route('CrearNE',$paciente->id) }}">Agregar Notas de Evolucion</a>
</div>
  <br>
  <br>

<!-- Eliminar expediente-->
@forelse ($paciente->D1 as $Doctor)
  <div class="modal fade" id="staticBackdrop{{$Doctor->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('historia_clinica.destroy',$Doctor->pivot->id) }}" method="POST">
            @csrf
            @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">¿Seguro que quiere eliminar el expdiente de : {{ $paciente->Nombre }} {{ $paciente->Apellido_Paterno }} {{ $paciente->Apellido_Materno }} ?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img img class="card-img-top" src="{{ asset('images/Delete.jpg') }}"
            alt="Card image cap">
            <input type="hidden" name="id_pacc" value="{{$paciente->id}}">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancerlar</button>
          <button type="submit" class="btn btn-danger">Confirmar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  @empty
  @endforelse

 <!--Eliminar nota -->
  @forelse ($pac_NE->NE1 as $Nose)
  <div class="modal fade" id="nota{{$Nose->pivot->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('NoEvu.destroy',$Nose->pivot->id) }}" method="POST">
            @csrf
            @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title" id="notaLabel">¿Seguro que quiere eliminar a este nota creada hace: {{ $Nose->pivot->created_at->diffForHumans() }}  ?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img img class="card-img-top" src="{{ asset('images/Delete.jpg') }}"
            alt="Card image cap">
            <input type="hidden" name="id_pacc" value="{{$paciente->id}}">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancerlar</button>
          <button type="submit" class="btn btn-danger">Confirmar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  @empty
  @endforelse

  <!-- Ver nota -->
  @forelse ($pac_NE->NE1 as $Nose)
  <div class="modal fade" id="viewData{{$Nose->pivot->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{route('print',$Nose->pivot->id)}}" method="post ">
        <div class="modal-header">
          <h5 class="modal-title" id="viewDataLabel">Paciente:  </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center>
                <!-- here esta la card-->
                <div class="card mb-3"  style="max-width: 540px; background-color:currentColor">
                    <!-- Card content -->
                    <div class="card-body elegant-color white-text rounded-bottom">
                    <!-- Social shares button -->
                    <a class="activator waves-effect mr-4" style="color:floralwhite"><i class="far fa-user"></i></a>
                    <!-- Title -->
                    <h4 class="card-title" style="color:floralwhite ; font-family: Courier New" >Nota evolutiva diagnosticada por: <strong>{{$Nose->Nombre}} {{$Nose->Apellido_Paterno}} {{$Nose->Apellido_Materno}}</strong></h4>
                    <hr class="hr-light" style="background-color: floralwhite">
                    <!-- Text -->
                        <div class="form-group">
                            <br>
                            <div class="form-group">
                                <p class="card-text" style="color: floralwhite; font-family: Courier New">Sintomas: {{ $Nose->pivot->Sintomas}}</p>
                            </div>
                            <div class="form-group">
                                <p class="card-text" style="color: floralwhite; font-family: Courier New">Signos: {{ $Nose->pivot->Signos}}</p>
                            </div>
                            <div class="justify-content-between align-items-center row">
                                <div class="col-md-6 mx-auto">
                                    <p class="card-text" style="color: floralwhite; font-family: Courier New">Diagnostico: {{ $Nose->pivot->Diagnostico}}</p>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <p class="card-text" style="color: floralwhite; font-family: Courier New">Tratamiento: {{ $Nose->pivot->Tratamiento}}</p>
                                </div>
                            </div>
                            <br>
                            <div class="justify-content-between align-items-center row">
                                <div class="col-md-6 mx-auto">
                                    <p class="card-text" style="color: floralwhite; font-family: Courier New">Fecha de esta Consulta : {{ $Nose->pivot->created_at->toFormattedDateString() }}</p>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <p class="card-text" style="color: floralwhite; font-family: Courier New">Ultima Consulta : {{ $Nose->pivot->created_at->diffForHumans() }}</p>
                                </div>
                            </div>
                            <br>
                        </div>
                        <!-- <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Hora de registro : {{ $paciente->created_at->diffForHumans() }}</p> -->
                    <!-- Link  href="{{url ('entrenadores/ayuda2',$paciente->id)}}"-->
                    <hr class="hr-light" style="background-color: floralwhite">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Imprimir &nbsp;&nbsp;&nbsp;<i class="fa fa-print"></i></button>
        </div>
        </form>
      </div>
    </div>
  </div>
  @empty

  @endforelse


@endsection
