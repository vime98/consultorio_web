@extends('layouts.app')
@section('content')
<br>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Historia Clinica</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('VerExpe',$nota->paciente_id) }}"> Atras</a>
        </div>
    </div>
</div>
<form action="{{ route('NoEvu.update',$nota->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <br>
       <div class="form-group">
           <label for="Sintomas">{{'Sintomas'}}</label>
           <textarea type="text" name="Sintomas" id="Sintomas" class="form-control input-sm"
           >{{ $nota->Sintomas}}</textarea>
       </div>
       <div class="form-group">
        <label for="Signos">{{'Signos'}}</label>
        <textarea type="text" name="Signos" id="Signos" class="form-control input-sm"
        >{{ $nota->Signos}}</textarea>
        </div>
       <div class="justify-content-between align-items-center row">
           <div class="col-md-6 mx-auto">
               <label for="Diagnostico">{{'Diagnostico'}}</label>
               <textarea type="text" name="Diagnostico" id="Diagnostico" class="form-control @error('Edad') is-invalid @enderror input-sm"
               >{{ $nota->Diagnostico}}</textarea>
           </div>
           <div class="col-md-6 mx-auto">
               <label for="Tratamiento">{{'Tratamiento'}}</label>
               <textarea  type="text" name="Tratamiento" id="Tratamiento" class="form-control @error('Direccion') is-invalid @enderror input-sm"
               >{{ $nota->Tratamiento}}</textarea>
           </div>
       </div>
       <br>
       <input type="hidden" name="id" value="{{ $nota->id }}">
        <input type="hidden" name="id_pacc" value="{{ $nota->paciente_id }}">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection
