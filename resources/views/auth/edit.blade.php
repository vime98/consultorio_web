@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Editar Usuario {{$users->Nombre}}</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('usuarios.index') }}"> Atras</a>
        </div>
    </div>
</div>

<form action="{{ route('usuarios.update', $users->id ) }}" method="POST">
    @csrf
    @method('PUT')
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="Nombre" class="form-control" placeholder="Nombre" value="{{ $users->Nombre }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Apellido Paterno:</strong>
                <input type="text" name="Apellido_Paterno" class="form-control" placeholder="Nombre" value="{{ $users->Apellido_Paterno }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Apellido Materno:</strong>
                <input type="text" name="Apellido_Materno" class="form-control" placeholder="Nombre" value="{{ $users->Apellido_Materno }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Edad:</strong>
                <input type="text" name="Edad" class="form-control" placeholder="Nombre" value="{{ $users->Edad }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Correo:</strong>
                <input type="text" name="email" class="form-control" placeholder="Correo" value="{{ $users->email }}">
            </div>
        </div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Contraseña:</strong>
                <input type="text" name="password" class="form-control" placeholder="Nombre" value="{{ $users->password }}">
            </div>
        </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Rol:</strong>
                <select name="rol_id" id="rol" class="form-control">
                    <option value={{$users->rol_id}}>
                    @foreach ($roles as $item)
                        @if ($item->id==$users->rol_id)
                            {{$item->Nombre_rol}}
                        @endif
                    @endforeach
                    </option>  
                    @foreach ($roles as $item)
                        @if ($item->id != $users->rol_id)
                        <option value={{$item->id}}>
                            {{$item->Nombre_rol}}
                        </option>                        
                        @endif
                    @endforeach 

                </select> 


            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection
