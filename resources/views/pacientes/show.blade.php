@extends('layouts.app')
@section('content')
<br>
<div class="form-group">
    <div class="justify-content-between align-items-center row">
        <div class="col-md-12 mx-auto">
            <h2>Editar Historia Clinica</h2>
        </div>
        <div class="col-md-2 ">
            <a class="btn btn-warning" href="{{ route('imprimir',$paciente->id) }}"> Imprimir</a>
        </div>
        <br>
        <br>
        <div class="col-md-2 ">
            <a class="btn btn-primary" href="{{route('VerMiExpediente',Auth::guard('paciente')->user()->id)}}"> Atras</a>
        </div>
    </div>
</div>
     <div class="form-group">
         <br>
         <div class="card text-white bg-dark mb-3" >
            <div class="card-header">Datos Generales</div>
            <div class="card-body">
                <div class="form-group">
                    <p class="card-text"> Nombre: {{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </p>
                </div>
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-2 mx-auto">
                        <p class="card-text"> Edad: {{$paciente->Edad}}</p>
                    </div>
                    <div class="col-md-10 mx-auto">
                        <p class="card-text"> Direccion: {{ $paciente->Direccion }}</p>
                    </div>
                </div>
            </div>
          </div>
        <br>
        <div class="card text-white bg-dark mb-3" >
            <div class="card-header"> Antecedentes heredofamiliares</div>
            <div class="card-body">
                <div class="form-group">
                    <p class="card-text"> _Abuelos Paternos: {{ $paciente->Ante_AbuelosP }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Abuelos Maternos: {{ $paciente->Ante_AbuelosM }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Tios: {{ $paciente->Ante_Tios }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Mama: {{ $paciente->Ante_Mama }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Papa: {{ $paciente->Ante_Papa }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Hermanos: {{ $paciente->Ante_Hermanos }}</p>
                </div>
            </div>
          </div>
        <div class="card text-white bg-dark mb-3" >
            <div class="card-header"> Antecedentes Personales No Patologicos</div>
            <div class="card-body">
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Fecha Nacimiento: {{ $paciente->Fecha_Nacimiento }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Lugar Nacimiento: {{ $paciente->Lugar_Nacimiento }}</p>
                    </div>
                    @if ($paciente->Estado_Civil == "Soltero")
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Estado Civil: Soltero </p>
                    </div>
                    @elseif($paciente->Estado_Civil == "Casado")
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Estado Civil: Casado </p>
                    </div>
                    @else
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Estado Civil: Viudo </p>
                    </div>
                    @endif
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Toxicomanias: {{ $paciente->Toxicomanias }}</p>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <p class="card-text"> _Residencia Actual: {{ $paciente->Residencia_actual }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Escolaridad: {{ $paciente->Escolaridad }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Habitos de Higiene: {{ $paciente->Habitos_Higiene }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> _Habitos de Dieta: {{ $paciente->Habitos_Dieta }}</p>
                </div>
            </div>
          </div>
        <div class="card text-white bg-dark mb-3" >
            <div class="card-header"> Antecedentes Personales Patologicos</div>
            <div class="card-body">
                <div class="form-group">
                    <p class="card-text"> Enfermedad de la Infancia: {{ $paciente->Enferme_infancia }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Cirugias: {{ $paciente->Cirugias }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Alergias: {{ $paciente->Alergias }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Medicamento que toma Actualmente: {{ $paciente->Medica_Actual }}</p>
                </div>
            </div>
          </div>
        <div class="card text-white bg-dark mb-3" >
            <div class="card-header"> Antecedentes Gineco Obstetricos</div>
            <div class="card-body">
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Menarca: {{ $paciente->Menarca }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Ritmo Menstrual: {{ $paciente->Ritmo_Menstrual }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> FUM: {{ $paciente->FUM }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Gestas: {{ $paciente->Gestas }}</p>
                    </div>
                </div>
                <br>
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Abortos: {{ $paciente->Abortos }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Cesareas: {{ $paciente->Cesareas }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> FUP: {{ $paciente->FUP }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> USA: {{ $paciente->USA }}</p>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <p class="card-text"> Uso de Anticonseptivos: {{ $paciente->Anticonseptivos }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Paras: {{ $paciente->Paras }}</p>
                </div>
            </div>
          </div>

        <div class="card text-white bg-dark mb-3" >
            <div class="card-header"> Exploracion</div>
            <div class="card-body">
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Peso: {{ $paciente->Peso }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Talla: {{ $paciente->Talla }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Cabeza: {{ $paciente->Cabeza }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Ojos: {{ $paciente->Ojos }}</p>
                    </div>
                </div>
                <br>
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Oidos: {{ $paciente->Oidos }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Nariz: {{ $paciente->Nariz }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Torax: {{ $paciente->Torax }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Forma: {{ $paciente->Forma }}</p>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <p class="card-text"> Boca: {{ $paciente->Boca }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Cuello: {{ $paciente->Cuello }}</p>
                </div>
                <div class="justify-content-between align-items-center row">
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Moviemientos Respiratorios: {{ $paciente->Mov_Respira }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> FR: {{ $paciente->FR }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> Ruidos Anormales: {{ $paciente->Ruido_Anormal }}</p>
                    </div>
                    <div class="col-md-3 mx-auto">
                        <p class="card-text"> FC: {{ $paciente->FC }}</p>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <p class="card-text"> Abdomen: {{ $paciente->Abdomen }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Extremidades Superiores: {{ $paciente->Extre_superior }}</p>
                </div>
                <div class="form-group">
                    <p class="card-text"> Extremidades Inferiores: {{ $paciente->Extre_inferior }}</p>
                </div>
            </div>
          </div>
    </div>
@endsection
