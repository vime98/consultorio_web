@extends('layouts.app')
@section('content')
<div class="container">
    <br>
    <div class="form-group">
        <div class="justify-content-between align-items-center row">
            <div class="col-md-4 mx-auto">
               <center><div class="card" style="max-width: 300px;">
                    @if ($paciente->Sexo == "M" )
                      <img src="{{ asset('images/M1.jpg') }}" class="card-img-top" alt="Card image cap">
                      @else
                      <img src="{{ asset('images/F1.png') }}" class="card-img-top" alt="Card image cap">
                      @endif
                    <div class="card-body">
                      <h5 class="card-title">Paciente: &nbsp;{{$paciente->Nombre}} {{$paciente->ApellidoPaterno}} {{$paciente->ApellidoMaterno}} </h5>
                      <p class="card-text">Correo: {{$paciente->email}}</p>
                      @if ($paciente->Sexo == "F" )
                      <p class="card-text">Sexo: Femenino</p>
                      @else
                      <p class="card-text">Sexo: Masculino</p>
                      @endif
                      <p class="card-text"><small class="text-muted">Fecha de alta: {{ $paciente->created_at->toFormattedDateString() }}</small></p>
                    </div>
                  </div>
                  <br>
            </div>
                @forelse ($paciente->D1 as $Doctor)
                <div class="col-md-8 mx-auto">
                <center>
                    <!-- here esta la card-->
                    <div class="card mb-3"  style="max-width: 540px; background-color:currentColor">
                        <!-- Card content -->
                        <div class="card-body elegant-color white-text rounded-bottom">
                        <!-- Social shares button -->
                        <a class="activator waves-effect mr-4" style="color:floralwhite"><i class="far fa-user"></i></a>
                        <!-- Title -->
                        <h4 class="card-title" style="color:floralwhite ; font-family: Courier New" >Doctor que registro el Expediente: &nbsp;{{ $Doctor->Nombre }} {{ $Doctor->Apellido_Paterno }} {{ $Doctor->Apellido_Materno }}</h4>
                        <hr class="hr-light" style="background-color: floralwhite">
                        <!-- Text -->
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Datos Expediente Paciente:</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Edad: {{$Doctor->pivot->Edad}} </p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Direccion: {{$Doctor->pivot->Direccion}} </p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Doctor Principal : {{ $paciente->user->Nombre }} {{ $paciente->user->Apellido_Paterno }} {{ $paciente->user->Apellido_Materno }}</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Fecha de registro del Paciente : {{ $paciente->created_at->toFormattedDateString() }}</p>
                            <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Tiempo que tiene el expediente : {{ $Doctor->pivot->created_at->diffForHumans() }}</p>
                            <!-- <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Hora de registro : {{ $paciente->created_at->diffForHumans() }}</p> -->
                        <!-- Link  href="{{url ('entrenadores/ayuda2',$paciente->id)}}"-->
                        <hr class="hr-light" style="background-color: floralwhite">
                        <div class="justify-content-between align-items-center row">
                            <div class="col-md-4 mx-auto">
                                <a href="{{ route('showMiExpediente',$Doctor->pivot->id) }}" class="white-text d-flex justify-content-end" style="color: floralwhite ; font-family: Courier New">
                                    <h5>Ver Expe <i class="far fa-eye"></i></h5>
                                </a>
                            </div>
                            <div class="col-md-4 mx-auto">
                                <a class="white-text d-flex justify-content-end" style="color: floralwhite ; font-family: Courier New" href="{{ route('imprimir',$Doctor->pivot->id) }}">
                                    <h5>Print Expe <i class="fas fa-print"></i></h5>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                @empty
                <br>
                <div class="col-md-8 mx-auto">
                    <p>No hay Expediente </p>
                </div>
                @endforelse
        </div>
        <br>
    </div>
  </div>
<div class="container">
    <br>
    <div class=" row row-cols-1 row-cols-md-3 row-cols-lg-4" >
        @forelse ($pac_NE->NE1 as $Nose)
        <div class="col mb-4" style="max-width: 100rem;" id="myDIV">
          <div class="card border-warning">
              <div class="card-header">Nota Evolutiva</div>
          <div class="card-body">
              <h5 class="card-title">Diagnosticado por: <strong>{{$Nose->Nombre}} {{$Nose->Apellido_Paterno}} {{$Nose->Apellido_Materno}}</strong></h5>
              <p class="card-text">Informacion:</p>
              <p class="card_text">Doctor Principal : {{ $paciente->user->Nombre }} {{ $paciente->user->Apellido_Paterno }} {{ $paciente->user->Apellido_Materno }}</p>
              <p class="card-text">Fecha de Ultima Consulta : {{ $Nose->pivot->created_at->toFormattedDateString() }}</p>
              <p class="card-text">Ultima Consulta : {{ $Nose->pivot->created_at->diffForHumans() }}</p>
              <div class="d-inline-flex">
                  <a class="btn btn-light" data-toggle="modal" data-target="#viewData{{$Nose->pivot->id}}">
                      <i class="fa fa-eye"></i></a>
             </div>
          </div>
          </div>
        </div>
        @empty
        <br>
        <div class="col-md-8 mx-auto">
                <p>No hay Notas</p>
        </div>
        @endforelse
      </div>
</div>

<!-- Ver nota -->
@forelse ($pac_NE->NE1 as $Nose)
<div class="modal fade" id="viewData{{$Nose->pivot->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="{{route('print',$Nose->pivot->id)}}" method="post ">
      <div class="modal-header">
        <h5 class="modal-title" id="viewDataLabel">Paciente:  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <center>
              <!-- here esta la card-->
              <div class="card mb-3"  style="max-width: 540px; background-color:currentColor">
                  <!-- Card content -->
                  <div class="card-body elegant-color white-text rounded-bottom">
                  <!-- Social shares button -->
                  <a class="activator waves-effect mr-4" style="color:floralwhite"><i class="far fa-user"></i></a>
                  <!-- Title -->
                  <h4 class="card-title" style="color:floralwhite ; font-family: Courier New" >Nota evolutiva diagnosticada por: <strong>{{$Nose->Nombre}} {{$Nose->Apellido_Paterno}} {{$Nose->Apellido_Materno}}</strong></h4>
                  <hr class="hr-light" style="background-color: floralwhite">
                  <!-- Text -->
                      <div class="form-group">
                          <br>
                          <div class="form-group">
                              <p class="card-text" style="color: floralwhite; font-family: Courier New">Sintomas: {{ $Nose->pivot->Sintomas}}</p>
                          </div>
                          <div class="form-group">
                              <p class="card-text" style="color: floralwhite; font-family: Courier New">Signos: {{ $Nose->pivot->Signos}}</p>
                          </div>
                          <div class="justify-content-between align-items-center row">
                              <div class="col-md-6 mx-auto">
                                  <p class="card-text" style="color: floralwhite; font-family: Courier New">Diagnostico: {{ $Nose->pivot->Diagnostico}}</p>
                              </div>
                              <div class="col-md-6 mx-auto">
                                  <p class="card-text" style="color: floralwhite; font-family: Courier New">Tratamiento: {{ $Nose->pivot->Tratamiento}}</p>
                              </div>
                          </div>
                          <br>
                          <div class="justify-content-between align-items-center row">
                              <div class="col-md-6 mx-auto">
                                  <p class="card-text" style="color: floralwhite; font-family: Courier New">Fecha de esta Consulta : {{ $Nose->pivot->created_at->toFormattedDateString() }}</p>
                              </div>
                              <div class="col-md-6 mx-auto">
                                  <p class="card-text" style="color: floralwhite; font-family: Courier New">Ultima Consulta : {{ $Nose->pivot->created_at->diffForHumans() }}</p>
                              </div>
                          </div>
                          <br>
                      </div>
                      <!-- <p class="card-text white-text mb-4" style="color: floralwhite; font-family: Courier New">Hora de registro : {{ $paciente->created_at->diffForHumans() }}</p> -->
                  <!-- Link  href="{{url ('entrenadores/ayuda2',$paciente->id)}}"-->
                  <hr class="hr-light" style="background-color: floralwhite">
                  </div>
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Imprimir &nbsp;&nbsp;&nbsp;<i class="fa fa-print"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>
@empty

@endforelse
@endsection
