@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Mis Pacientes</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('paciente.create') }}"> Agregar Nuevo Paciente</a>
        </div>
        <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ url('paciente/paccomp') }}"> Todos los Pacientes </a>
        </div>
    </div>
</div>
<br>
<div class="table-responsive">
  <input class="form-control mb-4" id="tableSearch2" type="text" placeholder="Type something to search list items">
    <table id="example" class="table table-striped hover table-dark" style="width:100%">
  <thead>
      <tr>
          <th><center>#</th>
          <th><center>Nombre</th>
          <th><center>Sexo</th>
          <th><center>Email</th>
          <th><center>Compartido</th>
          <th><center>Doctor</th>
          <th><center>Accion</th>
          <th><center>Expediente Medico</th>

      </tr>
  </thead>
  <tbody id="myTable">
      <!-- {{ $i = 0}} -->
      @forelse ($users->pacientes as $item)
      <tr>
        <td><center>{{ ++$i }}</td>
        <td><center>{{ $item->Nombre }}</td>
        @if ($item->Sex == "F" )
        <td><center>Femenino</td>
        @else
        <td><center>Masculino</td>
        @endif
        <td><center>{{ $item->email }}</td>
        @if ($item->compartido == 1)
        <td><center>Si</td>
        @else
        <td><center>No</td>
        @endif
        <td><center>{{ $users->Nombre }}</td>
        <td><center><div class="d-inline-flex">
            <a class="btn btn-warning" style="color: black" data-toggle="modal" data-target="#viewData{{$item->id}}">Ver</a>&nbsp;
            <a class="btn btn-primary"style="color: black" href="{{ route('paciente.edit',$item->id) }}">Editar</a>&nbsp;
            <a class="btn btn-danger"style="color: black" data-toggle="modal" data-target="#staticBackdrop{{$item->id}}" >Eliminar</a>
            </div>
        </td>
        <td><center>
            <div class="d-inline-flex">
            <a class="btn btn-warning" href="{{ route('VerExpe',$item->id) }}" >Expediente</a>
            </div>
        </td>
        </tr>
      @empty
      @endforelse
  </tbody>
  </table>
</div>


  @forelse ($users->pacientes as $item)
  <div class="modal fade" id="staticBackdrop{{$item->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('paciente.destroy',$item->id) }}" method="POST">
            @csrf
            @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">¿Seguro que quiere eliminar a este paciente: {{$item->Nombre}} {{$item->Apellido_Paterno}} {{$item->Apellido_Materno}} ?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img img class="card-img-top" src="{{ asset('images/Delete.jpg') }}"
            alt="Card image cap">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancerlar</button>
          <button type="submit" class="btn btn-danger">Confirmar</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  @empty
  @endforelse

  @forelse ($users->pacientes as $item)
  <div class="modal fade" id="viewData{{$item->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="viewDataLabel">Paciente: {{$item->nombre}} {{$item->a_paterno}} {{$item->a_materno}} </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p><Strong><h3>Nombre Completo:</h3></Strong> {{$item->Nombre}} {{$item->Apellido_Paterno}} {{$item->Apellido_Materno}}</p>
            @if ($item->Sex == "F" )
            <p><Strong><h3>Sexo:</h3></Strong> Femenino </p>
            @else
            <p><Strong><h3>Sexo:</h3></Strong> Masculino </p>
            @endif
            <p><Strong><h3>Correo:</h3></Strong> {{$item->email}} </p>
            <p><Strong><h3>Doctor Principal :</h3></Strong> {{ $item->user->Nombre }}</p>
            <p><Strong><h3>Fecha y hora de creacion :</h3></Strong> {{ $item->created_at }}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  @empty

  @endforelse


@endsection
