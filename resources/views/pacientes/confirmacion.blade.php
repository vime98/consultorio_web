@extends('layouts.app')
@extends('pacientes.index')
@section('modal')
<br>
    <div class="container" style="">
        <br>
        <center><div class="card" style="width: 18rem; background-color: darkred">
            <div class="card-body">
                <h3 style="color: white">¿Seguro que quiere eliminar a este paciente: {{$paciente->Nombre}} {{$paciente->Apellido_Paterno}} {{$paciente->Apellido_Materno}} ?</h3>
                <form action="{{ route('paciente.destroy',$paciente->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a class="btn btn-primary" href="{{ route('paciente.index') }}"> Cancelar</a>
                    <button type="submit" class="btn btn-success">Confirmar</button>
                </form>
            </div>
        </div>
        <br>
    </div>
@endsection
