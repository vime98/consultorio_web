@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Todos los Pacientes</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('paciente.index') }}"> Atras</a>
        </div>
    </div>
</div>
<br>

<div class="table-responsive">
    <table id="example" class="table table-striped hover table-dark" style="width:100%">
  <thead>
      <tr>
          <th><center>#</th>
          <th><center>Nombre</th>
          <th><center>Email</th>
          <th><center>User</th>
          <th><center>Expediente</th>


      </tr>
  </thead>
  <tbody>
      <!-- {{ $i = 0}} -->
      @forelse ($pacientes as $item)
      @if ($item->compartido ==1)
      <tr>
        <td><center>{{ ++$i }}</td>
        <td><center>{{ $item->Nombre }} {{ $item->Apellido_Paterno }} {{ $item->Apellido_Materno }}</td>
        <td><center>{{ $item->email }}</td>
        <td><center>{{ $item->user->Nombre }}</td>
        <td><center>
            <div class="d-inline-flex">
            <a class="btn btn-warning" href="{{ route('VerExpe',$item->id) }}" >Expediente</a>
            </div>
        </td>
    </tr>
      @endif
      @empty
      @endforelse
  </tbody>
  </table>
  </div>

@endsection
