@extends('layouts.app')
@section('content')
<div class="container">
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bienvenido paciente {{  Auth::guard('paciente')->user()->Nombre }}!
                    <div class="form-group row">
                        <label for="Nombre" class="col-md-4 col-form-label text-md-right"></label>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{route('VerMiExpediente',Auth::guard('paciente')->user()->id)}}"> Ver mi Expediente</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
