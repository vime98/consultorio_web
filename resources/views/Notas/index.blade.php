@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Mis Notas</h2>
        </div>
        {{--  <div class="pull-right">
            <a class="btn btn-success" href="{{ route('notas.create') }}"> Agregar Nuevo Paciente</a>
        </div>  --}}
        {{--  <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ url('paciente/paccomp') }}"> Todos los Pacientes </a>
        </div>
    </div>  --}}
</div>
<br>
<div class="table-responsive">
  <input class="form-control mb-4" id="tableSearch2" type="text" placeholder="Type something to search list items">
    <table id="example" class="table table-striped hover table-dark" style="width:100%">
  <thead>
      <tr>
          <th>#</th>
          <th>Asunto</th>
          <th>Doctor</th>
          <th>Accion</th>

      </tr>
  </thead>
  <tbody id="myTable">
    @forelse ($notas as $item)
      <tr>
        <td>{{ $loop->iteration  }}</td>
        <td>{{$item->Asunto}}</td>
        <td >
          <option value={{$item->id_Emisor}}>
            {{'Dr.'}} {{$item->Nombre}} 
          </option>
          </td>
        {{--  <td>{{ $item->email }}</td>
        <td>{{ $item->compartido }}</td>
        <td>{{ $users->Nombre }}</td>  --}}
      <td>
            <a class="btn btn-warning" data-toggle="modal" data-target="#viewData{{$item->id}}">Ver</a>
    </td>
    </tr>
      @empty

       @endforelse
  </tbody>
  </table>
  </div>

  @forelse ($notas as $item)
  <div class="modal fade" id="viewData{{$item->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="viewDataLabel">Asunto: {{$item->Asunto}} </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p><Strong><h3>Descripción:</h3></Strong> {{$item->Descripcion}}</p>
            <p><Strong><h3>Fecha y hora de creacion :</h3></Strong> {{ $item->created_at }}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  @empty

  @endforelse  
  <!-- Modal -->


@endsection
