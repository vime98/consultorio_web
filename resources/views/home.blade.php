@extends('layouts.app')

@section('content')
<div class="container">
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido Dr {{Auth::guard('web')->user()->Nombre}}!
                    <div class="form-group row">
                        <label for="Nombre" class="col-md-4 col-form-label text-md-right"></label>
                        <br>
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('paciente.index') }}"> Ver mis pacientes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
