<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriaClinicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historia_clinica', function (Blueprint $table) {
            $table->id();
            //no deberia poner pacientes, pero no quiero cambiar el modelo y controlador
            $table->unsignedBigInteger('pacientes_id');
            $table->foreign('pacientes_id')->references('id')->on('pacientes')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->unsigned();;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('Nombre');
            $table->string('Edad');
            $table->text('Direccion');
            $table->text('Ante_AbuelosP')->nullable();
            $table->text('Ante_AbuelosM')->nullable();
            $table->text('Ante_Tios')->nullable();
            $table->text('Ante_Papa')->nullable();
            $table->text('Ante_Mama')->nullable();
            $table->text('Ante_Hermanos')->nullable();
            $table->text('Fecha_Nacimiento')->nullable();
            $table->text('Lugar_Nacimiento')->nullable();
            $table->text('Residencia_actual')->nullable();
            $table->text('Escolaridad')->nullable();
            $table->text('Estado_Civil')->nullable();
            $table->text('Habitos_Higiene')->nullable();
            $table->text('Habitos_Dieta')->nullable();
            $table->text('Toxicomanias')->nullable();
            $table->text('Enferme_infancia')->nullable();
            $table->text('Cirugias')->nullable();
            $table->text('Alergias')->nullable();
            $table->text('Medica_Actual')->nullable();
            $table->text('Menarca')->nullable();
            $table->text('Ritmo_Menstrual')->nullable();
            $table->text('FUM')->nullable();
            $table->text('Gestas')->nullable();
            $table->text('Paras')->nullable();
            $table->text('Abortos')->nullable();
            $table->text('Cesareas')->nullable();
            $table->text('FUP')->nullable();
            $table->text('USA')->nullable();
            $table->text('Anticonseptivos')->nullable();
            $table->text('Peso')->nullable();
            $table->text('Talla')->nullable();
            $table->text('Cabeza')->nullable();
            $table->text('Ojos')->nullable();
            $table->text('Oidos')->nullable();
            $table->text('Nariz')->nullable();
            $table->text('Boca')->nullable();
            $table->text('Cuello')->nullable();
            $table->text('Torax')->nullable();
            $table->text('Forma')->nullable();
            $table->text('Mov_Respira')->nullable();
            $table->text('FR')->nullable();
            $table->text('Ruido_Anormal')->nullable();
            $table->text('FC')->nullable();
            $table->text('Abdomen')->nullable();
            $table->text('Extre_superior')->nullable();
            $table->text('Extre_inferior')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historia_clinica');
    }
}
