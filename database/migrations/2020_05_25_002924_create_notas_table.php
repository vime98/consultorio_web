<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('Id_Receptor')->unsigned();
            $table->bigInteger('Id_Emisor')->unsigned();
            $table->string('Asunto');
            $table->string('Descripcion');
            $table->timestamps();
            $table->foreign('Id_Receptor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('Id_Emisor')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
